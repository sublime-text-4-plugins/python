
# Sublime Text 4 Python override

This is a simple override for Sublime Text's default Python package. It has to be cloned/unpacked directly into **Packages** folder with the name **Python**.

- I use my own completions file for Python language. Instead of having definied completions in varous locations, I *blank* all snippets from the default package and use only my completions file.
- **Completion Rules.tmPreferences** file: I don't remember why I use this override.
- **Python.sublime-syntax** file: Added highlighting of interactive prompts (`>>>`) inside docstrings. This is a feature that I got from [MagicPython's](https://github.com/MagicStack/MagicPython) syntax.
